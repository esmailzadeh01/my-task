import React, { Component } from 'react';
import { createBrowserHistory } from "history";
import indexRoutes from "./routes";
import { Router, Route, Switch } from "react-router-dom";


const history = createBrowserHistory();
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }



    render() {
        return (
            <Router history={history}>
            
                <Switch>
                    {indexRoutes.map((prop, key) => {
                        return(
                           
                             <Route path={prop.path} component={prop.component} key={key} />
                             
                            );
                    })}
                </Switch>
            </Router>

        );
    }
}


export default App;