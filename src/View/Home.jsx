import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAdvertising } from '../Service/api';
import Advertising from './Advertising';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.getData();
    }

    getData() {
        getAdvertising()
            .then(function (res) {
                this.setState({ dataList: res });
            }.bind(this))
            .catch(err => {
                console.log(err, "err");
            });
    }




    render() {
        const { user } = this.props;
        const { dataList } = this.state;
        var notFound = <p>داده ای برای نمایش یافت نشد</p>
        return (
            <div>
                <div className="header text-right p-2 bg-primary text-light"> <span>{user.username}</span> خوش امدی</div>
                <div className="container-fluid">
                    <div className="row justify-content-center text-center">
                        {
                            dataList ?
                                dataList.result.map((prop, key) => {
                                    return (
                                        <div className="col-4" key={key}>
                                            <Advertising dataList={prop} />
                                        </div>
                                    )
                                })
                                : notFound
                        }
                    </div>
                </div>
            </div>


        );
    }
}


const mapStateToProps = state => ({
    user: state.user
});


export default connect(mapStateToProps, null)(Home);