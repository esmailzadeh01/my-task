import React, { Component } from 'react';

class Advertising extends Component {
    render() {
        const { dataList } = this.props;
        return (
            <div>
                <div className="card my-2">
                    <img src="" alt="" className="card-item" />
                    <div className="card-body">
                        <h5 className="card-title">{dataList.branch.addressText}</h5>
                        <p className="card-text">{dataList.branch.description}</p>
                        <div className="row">

                            <div className="col-6">{dataList.customer.firstName}</div>
                            <div className="col-6">{dataList.customer.lastName}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Advertising;