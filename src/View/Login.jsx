import React, { Component } from 'react';
import { LOGIN } from '../redux/constrants/actionTypes';
import { connect } from 'react-redux';
import Swal from 'sweetalert2';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {};

    }

    doLogin = () => {
        console.log(this.state.email, this.state.password)
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    doLogin = () => {

        if (this.state.email && this.state.password) {
            const user = {
                "username": this.state.email,
                "password": this.state.password
            }
            localStorage.setItem("username", user.username);

            this.props.userAdd(user);
            this.props.history.push("/home");
            

        } else {
            return Swal.fire({
                type: 'error',
                showConfirmButton: true,
                timer: 1500,
                title: 'خطا',
                text: `لطفا فیلدهای ورود را به درستی پر کنید`,
            });
        }

    }


    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-4 mt-5 shadow-sm bg-white p-2 text-center">
                        <h5>ورود به سایت</h5>
                        <div className="text-right mt-2">
                            <div className="form-group">
                                <input type="email" name="email" className="form-control text-right" id="mail" aria-describedby="emailHelp" placeholder="آدرس پست الکترونیک" onChange={this.handleChange} />
                            </div>
                            <div className="form-group">
                                <input type="password" name="password" className="form-control text-right" id="pass" placeholder="گذرواژه" onChange={this.handleChange} />
                            </div>
                        </div>
                        <button type="submit" className="btn btn-primary" onClick={this.doLogin}>ورود</button>
                    </div>
                </div>
            </div>
        );
    }
}


const mapDispatchToProps = dispatch => ({
    userAdd: (newData) => dispatch({ type: LOGIN, newData }),
});

export default connect(null, mapDispatchToProps)(Login);
