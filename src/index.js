import React , {Component} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './asstes/css/style.css';
import App from './App';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';
import { createBrowserHistory } from "history";
import { store , persistor } from './redux/store';
import { PersistGate } from 'redux-persist/integration/react';


const history = createBrowserHistory();
export default class Root extends Component {
    constructor(props) {
            super(props);
            this.state = {}
        }


    render() {
        return (
            <Provider store = { store } >
                  <PersistGate persistor={persistor}>
                  <App />
                  </PersistGate>
            
            </Provider>     
        )
    }
}



ReactDOM.render( < Root /> , document.getElementById('root'));


registerServiceWorker();
