import axios from 'axios';

export const API_URL = `http://task.asanshahr.com`;


axios.defaults.baseURL = API_URL;
// axios.defaults.timeout = 1200;
// axios.defaults.headers.common.Accept = 'application/json';
// axios.defaults.headers.common['Content-Type'] = 'application/json';

axios.interceptors.response.use(
    response => {

        return response;
    },
    error => {

        return Promise.reject(error);
    });



export const getAdvertising = async () => {
    let res = await axios.get();
    return res.data;
};

