import Login from "../View/Login";
import Home from "../View/Home";

const indexRoutes = [
	{ path: "/home", component: Home },
	{ path: "", component : Login },
];


export default indexRoutes;