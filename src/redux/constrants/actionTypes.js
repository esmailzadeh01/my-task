export const APP_LOAD = 'APP_LOAD';
export const REDIRECT = 'REDIRECT';
export const ROUTE_CHANGED = 'ROUTE_CHANGED';
export const LOGIN = 'LOGIN';
export const LOG_OUT = 'LOG_OUT';
