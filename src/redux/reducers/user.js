import {
    LOGIN,
    LOG_OUT,
  } from '../constrants/actionTypes';
  
  export default (state = {}, action) => {
    switch (action.type) {
      case LOGIN:
        return {
          ...action.newData
        };
      case LOG_OUT:
        return null;
      default:
        return state;
    }
  };